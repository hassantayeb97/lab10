Hassan Asim Tayeb 1936820

Answer Q2:
the template methods in Transaction abstract class are set to final, but the invoked methods inside the template -( such as ShowDialog(), CloseDialog(), and Perform() )- are overriden in the concrete classes. When you create new object of Transaction, you can choose between Balance (Non-sensitive transaction) and Transfer (Sensetive transaction), which differs in implementation, by Typing "Transaction name = new Balance/Transfer". According to the type, the implementation that is overriden with it will be performed.
